import React, { FC } from 'react';


interface ComponentsMyComponentProps {}

const ComponentsMyComponent: FC<ComponentsMyComponentProps> = () => (
  <div>
    ComponentsMyComponent Component
  </div>
);

export default ComponentsMyComponent;
